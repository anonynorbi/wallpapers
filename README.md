# Wallpapers

![Screenshot of my wallpapers in system settings](https://gitlab.com/anonynorbi/dotfiles/-/raw/main/.screenshots/dotfiles00.png) 

My collection of wallpapers I collected over the years. Feel free to use them as you wish.

## Where Did I Get These?
I find wallpapers in a number of different locations but good places to check out include [Wallpaper Cave](https://wallpapercave.com), [/wg/](https://4chan.org/wg) and [Imgur](https://imgur.com). Some of the wallpapers were probably included in default wallpaper packages from various Linux distributions that I have installed over the years.

## Style of Wallpapers
The vast majority of these wallpapers are space, nature and landscape photos. There are a few abstract art wallpapers mixed in. Some of them are also Arch Linux pride backgrounds from the official repo [archlinux/archlinux-wallpaper](https://gitlab.archlinux.org/archlinux/packaging/packages/archlinux-wallpaper).

## Ownership
Because I downloaded most of these from sites like Wallpaper Cave, /wg/ and Imgur, I have no way of knowing if there is a copyright on these images. If you find an image hosted in this repository that is yours and of limited use, please let me know and I will remove it.
